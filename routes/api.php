<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => 'auth:sanctum'], function () {
    //ROUTE PERUSAHAAN
    Route::get('/Perusahaan',  'App\Http\Controllers\PerusahaanController@index');
    Route::post('Perusahaan',  'App\Http\Controllers\PerusahaanController@store');
    Route::get('Perusahaan/Show/{id}',  'App\Http\Controllers\PerusahaanController@show');
    Route::patch('/Perusahaan/Update/{id}',  'App\Http\Controllers\PerusahaanController@update');
    Route::delete('/Perusahaan/Destroy/{id}',  'App\Http\Controllers\PerusahaanController@destroy');


    //ROUTE SATUAN
    Route::get('/Satuan',  'App\Http\Controllers\SatuanController@index');
    Route::post('/Satuan',  'App\Http\Controllers\SatuanController@store');
    Route::get('/Satuan/Show/{id}',  'App\Http\Controllers\SatuanController@show');
    Route::patch('/Satuan/Update/{id}',  'App\Http\Controllers\SatuanController@update');
    Route::delete('/Satuan/Destroy/{id}',  'App\Http\Controllers\SatuanController@destroy');

    //ROUTE KATEGORI
    Route::get('/Kategori',  'App\Http\Controllers\KategoriController@index');
    Route::post('/Kategori',  'App\Http\Controllers\KategoriController@store');
    Route::get('/Kategori/Show/{id}',  'App\Http\Controllers\KategoriController@show');
    Route::patch('/Kategori/Update/{id}',  'App\Http\Controllers\KategoriController@update');
    Route::delete('/Kategori/Destroy/{id}',  'App\Http\Controllers\KategoriController@destroy');

    //ROUTE SUPPLIER
    //ERROR ON UPDATE SUPPLIER
    Route::get('/Supplier',  'App\Http\Controllers\SupplierController@index');
    Route::post('/Supplier',  'App\Http\Controllers\SupplierController@store');
    Route::get('/Supplier/Show/{id}',  'App\Http\Controllers\SupplierController@show');
    Route::patch('/Supplier/Update/{id}',  'App\Http\Controllers\SupplierController@update');
    Route::delete('/Supplier/Destroy/{id}',  'App\Http\Controllers\SupplierController@destroy');

    //ROUTE PELANGGAN
    Route::get('/Pelanggan',  'App\Http\Controllers\PelangganController@index');
    Route::post('/Pelanggan',  'App\Http\Controllers\PelangganController@store');
    Route::get('/Pelanggan/Show/{id}',  'App\Http\Controllers\PelangganController@show');
    Route::patch('/Pelanggan/Update/{id}',  'App\Http\Controllers\PelangganController@update');
    Route::delete('/Pelanggan/Destroy/{id}',  'App\Http\Controllers\PelangganController@destroy');

    //ROUTE USER
    Route::get('/User', 'App\Http\Controllers\UserController@index');
    Route::post('/User', 'App\Http\Controllers\UserController@store');
    Route::get('/User/Show/{id}', 'App\Http\Controllers\UserController@show');
    Route::patch('/User/Update/{id}', 'App\Http\Controllers\UserController@update');
    Route::delete('/User/Destroy/{id}', 'App\Http\Controllers\UserController@destroy');

    //ROUTE BARANG
    Route::get('/Barang', 'App\Http\Controllers\BarangController@index');
    Route::post('/Barang', 'App\Http\Controllers\BarangController@store');
    Route::get('/Barang/Show/{id}', 'App\Http\Controllers\BarangController@show');
    Route::patch('/Barang/Update/{id}', 'App\Http\Controllers\BarangController@update');
    Route::delete('/Barang/Destroy/{id}', 'App\Http\Controllers\BarangController@destroy');


    //ROUTE MEREK
    Route::get('/Merek', 'App\Http\Controllers\MerekController@index');
    Route::post('/Merek', 'App\Http\Controllers\MerekController@store');
    Route::get('/Merek/Show/{id}', 'App\Http\Controllers\MerekController@show');
    Route::patch('/Merek/Update/{id}', 'App\Http\Controllers\MerekController@update');
    Route::delete('/Merek/Destroy/{id}', 'App\Http\Controllers\MerekController@destroy');

    Route::get('/KasMasuk', 'App\Http\Controllers\KasMasukController@index');
    Route::post('/KasMasuk', 'App\Http\Controllers\KasMasukController@store');
    Route::get('/KasMasuk/Show/{id}', 'App\Http\Controllers\KasMasukController@show');
    Route::patch('/KasMasuk/Update/{id}', 'App\Http\Controllers\KasMasukController@update');
    Route::delete('/KasMasuk/Destroy/{id}', 'App\Http\Controllers\KasMasukController@destroy');

    Route::get('/KasKeluar', 'App\Http\Controllers\KasKeluarController@index');
    Route::post('/KasKeluar', 'App\Http\Controllers\KasKeluarController@store');
    Route::get('/KasKeluar/Show/{id}', 'App\Http\Controllers\KasKeluarController@show');
    Route::patch('/KasKeluar/Update/{id}', 'App\Http\Controllers\KasKeluarController@update');
    Route::delete('/KasKeluar/Destroy/{id}', 'App\Http\Controllers\KasKeluarController@destroy');

    //ROUTE TRANSAKSI PENJUALAN
    Route::get('/Transaksi/Penjualan', 'App\Http\Controllers\PenjualanController@index');
    Route::post('/Transaksi/Penjualan', 'App\Http\Controllers\PenjualanController@store');
    Route::get('/Transaksi/Penjualan/Show/{id}', 'App\Http\Controllers\PenjualanController@show');

    Route::get('/Logout', 'App\Http\Controllers\UserController@logout');
});
    
//api LOGIN
Route::post('/Login', 'App\Http\Controllers\UserController@login')->name('Login');

//api REGISTER
Route::post('/Register', 'App\Http\Controllers\PerusahaanController@store');