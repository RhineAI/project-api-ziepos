<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransaksiPenjualanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id"=> $this->id,
            "tgl"=> $this->tgl,
            "pelanggan"=> $this->whenLoaded('pelanggan'),
            "total_harga"=> $this->total_harga,
            "total_bayar"=> $this->total_bayar,
            "keuntungan"=> $this->keuntungan,
            "kembalian"=> $this->kembalian,
            "jenis_pembayaran"=> $this->jenis_pembayaran,
            "dp"=> $this->dp,
            "sisa"=> $this->sisa,
            "user"=> $this->whenLoaded('user'),
            'perusahaan' => $this->whenLoaded('perusahaan'),
            "created_at" => date_format($this->created_at, "Y-m-d H:i:s"),
            "updated_at" => date_format($this->updated_at, "Y-m-d H:i:s"),
        ];
    }
}
