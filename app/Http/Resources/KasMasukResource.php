<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class KasMasukResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tgl' => $this->tgl,
            'jumlah' => $this->jumlah,
            'keterangan' => $this->keterangan,
            'user' => $this->whenLoaded('user'),
            'perusahaan' => $this->whenLoaded('perusahaan'),
            "created_at" => date_format($this->created_at, "Y-m-d H:i:s"),
            "updated_at" => date_format($this->updated_at, "Y-m-d H:i:s"),
        ];
    }
}
