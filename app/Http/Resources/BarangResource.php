<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BarangResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
          "id" => $this->id,
          "kode" => $this->kode,
          "nama" => $this->nama,
          "barcode" => $this->barcode,
          "kategori" => $this->whenLoaded('kategori'),
          "supplier" => $this->whenLoaded('supplier'),
          "satuan" => $this->whenLoaded('satuan'),
          "merek" => $this->whenLoaded('merek'),
          "perusahaan" => $this->whenLoaded('perusahaan'),
          "tgl" => $this->tgl,
          "stock" => $this->stock,
          "stock_minimal" => $this->stock_minimal,
          "harga_beli" => $this->harga_beli,
          "keuntungan" => $this->keuntungan,
          "keterangan" => $this->keterangan,
          "status" => $this->status,
          "created_at" => date_format($this->created_at, "Y-m-d H:i:s"),
          "updated_at" => date_format($this->updated_at, "Y-m-d H:i:s"),
        ];
    }
}
