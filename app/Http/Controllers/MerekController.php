<?php

namespace App\Http\Controllers;

use App\Models\Merek;
use App\Helpers\ApiFormatter;
// use Illuminate\Routing\Controller;
use App\Http\Requests\StoreMerekRequest;
use App\Http\Requests\UpdateMerekRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\MerekResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MerekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $merek = Merek::all()->where('id_perusahaan', auth()->user()->id_perusahaan);
        $data = MerekResource::collection($merek->loadMissing('perusahaan'));

        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMerekRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'nama' =>'required',
                'id_perusahaan' => 'required',
            ]);
            $merek = Merek::create([
                'nama' =>$request->nama,
                'id_perusahaan' => auth()->user()->id_perusahaan,
            ]);
        
            $data = new MerekResource($merek->loadMissing('perusahaan'));
            DB::commit();
        
            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
    
        }catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Merek  $merek
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $merek = Merek::where('id', $id)->first();
        $data = new MerekResource($merek->loadMissing('perusahaan'));
    
        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Merek  $merek
     * @return \Illuminate\Http\Response
     */
    public function edit(Merek $merek)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMerekRequest  $request
     * @param  \App\Models\Merek  $merek
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'nama'=> 'required',
                'id_perusahaan' => 'required',
            ]);

            $merek = Merek::findOrFail($id);
            $merek->update([
                'nama' => $request->nama,
                'id_perusahaan' => auth()->user()->id_perusahaan,
            ]);

            $data = new MerekResource($merek->loadMissing('perusahaan'));
            DB::commit();

            if($merek) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan query');
            }
    
        } catch (\Exception $error) {
            DB::rollBack();
;            return ApiFormatter::createApi(400,'Kesalahan query '. $error->getMessage());
        }          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Merek  $merek
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $merek = Merek::findOrFail($id);

            $data = $merek->delete();
            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan query');
            }
        } catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan query '. $error->getMessage());
        }   
    }
}
