<?php

namespace App\Http\Controllers;

use App\Helpers\ApiFormatter;
use App\Models\Pelanggan;
// use Illuminate\Routing\Controller;
use App\Http\Requests\StorePelangganRequest;
use App\Http\Requests\UpdatePelangganRequest;

// use App\Helpers\ApiFormatter;
// use App\Models\Pelanggan;
// use App\Http\Requests\StorePelangganRequest;
// use App\Http\Requests\UpdatePelangganRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\PelangganResource;
use Exception;
use Illuminate\Support\Facades\DB;

class PelangganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pelanggan = Pelanggan::all()->where('id_perusahaan', auth()->user()->id_perusahaan);
        $data = PelangganResource::collection($pelanggan->loadMissing('perusahaan')); 

        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePelangganRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePelangganRequest $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'nama' =>'required',
                'alamat' => 'required',
                'tlp' => 'required',
                'jenis_kelamin' => 'required',
                'id_perusahaan' => 'required'
            ]);
            $pelanggan = Pelanggan::create([
                'nama' =>$request->nama,
                'alamat' => $request->alamat,
                'tlp' =>$request->tlp,
                'jenis_kelamin' => $request->jenis_kelamin,
                'id_perusahaan' => auth()->user()->id_perusahaan
            ]);
        
            $data = new PelangganResource($pelanggan->loadMissing('perusahaan'));
            DB::commit();
            
            if($data) {
                return ApiFormatter::createApi(200, 'Input Data Berhasil', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
        } catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pelanggan = Pelanggan::where('id', $id)->first();
        $data = new PelangganResource($pelanggan->loadMissing('perusahaan'));
    
        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function edit(Pelanggan $pelanggan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePelangganRequest  $request
     * @param  \App\Models\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePelangganRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'nama' =>'required',
                'alamat' => 'required',
                'tlp' => 'required',
                'jenis_kelamin' => 'required',
                'id_perusahaan' => 'required'
            ]);
            
            $pelanggan = Pelanggan::findOrFail($id);
                
            $pelanggan->update([
                'id' =>$request->id,
                'nama' =>$request->nama,
                'alamat' => $request->alamat,
                'tlp' =>$request->tlp,
                'jenis_kelamin' => $request->jenis_kelamin,
                'id_perusahaan' => auth()->user()->id_perusahaan
            ]);
        
            $data = new PelangganResource($pelanggan->loadMissing('perusahaan'));
            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'Update Data Berhasil', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
        } catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }         
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $pelanggan = Pelanggan::findOrFail($id);

            $data = $pelanggan->delete();
            DB::commit();
            
            if($data) {
                return ApiFormatter::createApi(200, 'Hapus Data Berhasil', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
        } catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }  
    }
}
