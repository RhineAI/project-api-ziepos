<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
// use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;
use App\Helpers\ApiFormatter;
use App\Http\Resources\SupplierResource;
use Exception;
use Illuminate\Routing\Controller;
// use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// use App\Helpers\ApiFormatter;
// use App\Models\Perusahaan;
// use App\Models\Supplier;
// use Illuminate\Http\Request;


class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplier = Supplier::all()->where('id_perusahaan', auth()->user()->id_perusahaan);
        $data = SupplierResource::collection($supplier->loadMissing('perusahaan'));

        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'nama' =>'required',
                'alamat' => 'required',
                'tlp' => 'required',
                'salesman' => 'required',
                'bank' => 'required',
                'no_rekening' => 'required',
                'id_perusahaan' => 'required',
            ]);

            $supplier = Supplier::create([
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'tlp' => $request->tlp,
                'salesman' => $request->salesman,
                'bank' => $request->bank,
                'no_rekening' => $request->no_rekening,
                'id_perusahaan' => auth()->user()->id_perusahaan,
            ]);
        
            $data = new SupplierResource($supplier->loadMissing('perusahaan'));
            DB::commit();
        
            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
        }catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier = Supplier::where('id', $id)->first();
        $data = new SupplierResource($supplier->loadMissing('perusahaan'));
        // $data = Mahasiswa::where('id', '=', $id)->get();
    
        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        DB::beginTransaction();
        try {
            // $request->validate([
            //     'nama' =>'required',
            //     'alamat' => 'required',
            //     'tlp' => 'required',
            //     'salesman' => 'required',
            //     'bank' => 'required',
            //     'no_rekening' => 'required',
            //     'id_perusahaan' => 'required',
            // ]);

            $supplier = Supplier::findOrFail($id);

            $supplier->update([
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'tlp' => $request->tlp,
                'salesman' => $request->salesman,
                'bank' => $request->bank,
                'no_rekening' => $request->no_rekening,
                'id_perusahaan' => auth()->user()->id_perusahaan,
            ]);
        
            $data = new SupplierResource($supplier->loadMissing('perusahaan'));
            DB::commit();
        
            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
        } catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $supplier = Supplier::findOrFail($id);

            $data = $supplier->delete();
            DB::commit();
        
            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
        } catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }   
    }
}
