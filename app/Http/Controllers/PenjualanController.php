<?php

namespace App\Http\Controllers;

use App\Models\Penjualan;
use App\Models\Perusahaan;
use App\Models\Supplier;
use App\Models\User;
use App\Helpers\ApiFormatter;
// use Illuminate\Routing\Controller;
use App\Http\Requests\StorePenjualanRequest;
use App\Http\Requests\UpdatePenjualanRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\TransaksiPenjualanResource;
use App\Models\Barang;
use App\Models\DetailPenjualan;
use App\Models\KasMasuk;
use App\Models\Piutang;
use App\Models\TransaksiPenjualan;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use PDOException;

class PenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = TransaksiPenjualan::all()->where('id_perusahaan', auth()->user()->id_perusahaan);
        $data = TransaksiPenjualanResource::collection($transaksi->loadMissing(['pelanggan', 'user', 'perusahaan']));

        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function NextId($tgl){
        $pieces = explode("-",$tgl);
        $yy=$pieces[0]; 
        $mm=$pieces[1]; 
        $dd=$pieces[2]; 
        $tgl=$yy.$mm.$dd;
        
        $result= TransaksiPenjualan::select(DB::raw('max(id)+1 AS nextid'))->orderBy('id', 'DESC')->where('t_transaksi_penjualan.id_perusahaan', auth()->user()->id_perusahaan)->first();
        if($tgl==substr($result->nextid,0,8)){
            $nextid=$result->nextid; 
        } else{
            $nextid=$tgl.'001';
        }

        return $nextid;
    }

    public function checkPrice($value)
    {
        if (gettype($value) == "string") {
            $temp = 0;
            for ($i = 0; $i < strlen($value); $i++) {
                if ((isset($value[$i]) == true && $value[$i] != ".") && $value[$i] != ",") {
                    $temp = ($temp * 10) + (int)$value[$i];
                }
            }
            return $temp;
        } else {
            return $value;
        }
    }

    public function store(StorePenjualanRequest $request)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::beginTransaction();
        try {
            if($request->kembali < 0){
                return ApiFormatter::createApi(400, 'Uang Bayar Kurang');
            } else {
                $penjualanBaru = new TransaksiPenjualan();
                $id = $this->NextId(date('Y-m-d'));

                $penjualanBaru->id = $id;
                $penjualanBaru->tgl = date('Y-m-d');
                $penjualanBaru->id_pelanggan = $request->id_pelanggan;
                $penjualanBaru->total_harga = $this->checkPrice($request->total_harga);
                if($request->jenis_pembayaran == '1') {
                    $penjualanBaru->total_bayar = $this->checkPrice($request->total_bayar);
                    $penjualanBaru->sisa = 0;
                } else {
                    $penjualanBaru->total_bayar = $this->checkPrice($request->dp);
                    $penjualanBaru->sisa = $this->checkPrice($request->sisa);
                }

                $penjualanBaru->kembalian = $this->checkPrice($request->kembali);
                $penjualanBaru->dp = $this->checkPrice($request->dp);

                $penjualanBaru->jenis_pembayaran = $request->jenis_pembayaran;
                $penjualanBaru->id_user = auth()->user()->id;
                $penjualanBaru->id_perusahaan = auth()->user()->id_perusahaan;

                $perusahaan = Perusahaan::where('id', auth()->user()->id_perusahaan)->first();
                $limit = TransaksiPenjualan::where('id_perusahaan', auth()->user()->id_perusahaan)->where('tgl', date('Y-m-d'))->count();

                foreach($request->item as $barang){
                    $penjualanBaru->keuntungan += $barang['keuntungan'] * $barang['qty'];

                    if($perusahaan->grade == 1) {
                        if($limit < 5 ) {
                            $penjualanBaru->save();
                        }else {
                            return ApiFormatter::createApi(400, 'Sudah mencapai limit barang, Naikan levelmu terlebih dahulu!');
                        }
                    } elseif($perusahaan->grade == 2) {
                        if($limit < 50 ) {
                            $penjualanBaru->save();
                        }else {
                            return ApiFormatter::createApi(400, 'Sudah mencapai limit barang, Naikan levelmu terlebih dahulu!');
                        }
                    } elseif($perusahaan->grade == 3) {
                        $penjualanBaru->save();
                    } else{
                        return ApiFormatter::createApi(400, 'Sudah mencapai limit barang, Hubungi Kami Untuk Informasi Lebih Lanjut');
                    }

                    $detPenjualanBaru = new DetailPenjualan(); 
                    $detPenjualanBaru->tgl = date('Y-m-d');
                    $detPenjualanBaru->id_penjualan = $id;
                    $detPenjualanBaru->id_barang = $barang['id_barang'];
                    $detPenjualanBaru->qty = $barang['qty'];
                    if($barang['discount']){
                        $detPenjualanBaru->diskon = $barang['discount'];
                    } else {
                        $detPenjualanBaru->diskon = 0;
                    }
                    $detPenjualanBaru->harga_beli = $barang['harga_beli'];
                    $detPenjualanBaru->harga_jual = $barang['harga_jual'];
                    $detPenjualanBaru->id_perusahaan =  $penjualanBaru->id_perusahaan;
                    $detPenjualanBaru->save();
                    
                    $barangUpdate = Barang::find($barang['id_barang']);
                    $barangUpdate->stock -= $barang['qty'];
                    $barangUpdate->update();
                }

                if($request->jenis_pembayaran == 1){
                    $kasMasuk = new KasMasuk();
                    $kasMasuk->tgl = now();
                    $kasMasuk->jumlah = $this->checkPrice($request->total_harga); 
                    $kasMasuk->id_user = auth()->user()->id;
                    $kasMasuk->id_perusahaan = auth()->user()->id_perusahaan;
                    $kasMasuk->keterangan = 'Transaksi Penjualan';
                    $kasMasuk->save();
                } else if ($request->jenis_pembayaran == 2){
                    $pembayaranBaru = new Piutang();
                    $pembayaranBaru->id_penjualan = $id;
                    $pembayaranBaru->tgl = date('Ymd');
                    $pembayaranBaru->total_bayar = $this->checkPrice($request->dp);
                    $pembayaranBaru->id_user = auth()->user()->id;
                    $pembayaranBaru->id_perusahaan = auth()->user()->id_perusahaan;
                    $pembayaranBaru->save();

                    // Lalu total DP masuk akan masuk ke Kas Masuk 
                    $kasMasuk = new KasMasuk();
                    $kasMasuk->tgl = now();
                    $kasMasuk->jumlah = $this->checkPrice($request->dp); 
                    $kasMasuk->id_user = auth()->user()->id;
                    $kasMasuk->id_perusahaan = auth()->user()->id_perusahaan;
                    $kasMasuk->keterangan = 'DP Transaksi Penjualan';
                    $kasMasuk->save();
                }

                $data = [
                    
                ];
                DB::commit();

                return ApiFormatter::createApi(200, 'Transa', $data);

            }   
        } catch (QueryException | PDOException | \Exception){
            DB::rollBack();
            return back()->with('error', 'Terjadi Kesalahan Server!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penjualan = TransaksiPenjualan::where('id', $id)->first();
        if($penjualan){
            $data = new TransaksiPenjualanResource($penjualan->loadMissing(['pelanggan', 'user', 'perusahaan']));
            
            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Tidak ada Data');
            }
        } else {
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePenjualanRequest  $request
     * @param  \App\Models\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePenjualanRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
