<?php

namespace App\Http\Controllers;

use App\Helpers\ApiFormatter;
use App\Models\KasKeluar;
use App\Http\Controllers\Controller;
use App\Http\Resources\KasKeluarResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KasKeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = kasKeluar::all()->where('id_perusahaan', auth()->user()->id_perusahaan);

        if($data) {
            return KasKeluarResource::collection($data->loadMissing(['user', 'perusahaan']));
            // return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreKasKeluarControllerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'tgl' => 'required',
                'jumlah' => 'required|numeric',
                'keperluan' => 'required'
            ]);


            $kasKeluar = kasKeluar::create([
                'tgl' => $request->tgl,
                'jumlah' => $request->jumlah,
                'keperluan' => $request->keperluan,
                'id_user' => auth()->user()->id,
                'id_perusahaan' => auth()->user()->id_perusahaan,
            ]);
    
            $data = new KasKeluarResource($kasKeluar->loadMissing(['user', 'perusahaan']));
            DB::commit();
        
            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
    
        }   catch (Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KasKeluarController  $kasKeluarController
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kasKeluar = KasKeluar::where('id', $id)->first();
        $data = new KasKeluarResource($kasKeluar->loadMissing(['user', 'perusahaan']));

        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Data Tidak Ditemukan');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KasKeluarController  $kasKeluarController
     * @return \Illuminate\Http\Response
     */
    public function edit(KasKeluarController $kasKeluarController)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateKasKeluarControllerRequest  $request
     * @param  \App\Models\KasKeluarController  $kasKeluarController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'tgl' => 'required',
                'jumlah' => 'required',
                'keperluan' => 'required'
            ]);

            $kasKeluar = kasKeluar::findOrFail($id);

            $kasKeluar->update([
                'tgl' => $request->tgl,
                'jumlah' => $request->jumlah,
                'keperluan' => $request->keperluan,
                'id_user' => auth()->user()->id,
                'id_perusahaan' => auth()->user()->id_perusahaan,
            ]);
    
            $data = new KasKeluarResource($kasKeluar->loadMissing(['user', 'perusahaan']));
            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'Update Berhasil', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query ');
            }
    
        }catch (Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }          
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $kasKeluar = KasKeluar::findOrFail($id);

            $data = $kasKeluar->delete();
            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
        } catch (Exception $error){
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }
        
    }
}
