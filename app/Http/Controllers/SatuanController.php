<?php

namespace App\Http\Controllers;
use App\Helpers\ApiFormatter;
use App\Http\Controllers\Controller;
use App\Models\Perusahaan;
use App\Models\Satuan;
use App\Http\Requests\StoreSatuanRequest;
use App\Http\Requests\UpdateSatuanRequest;
use App\Http\Resources\SatuanResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SatuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $satuan = Satuan::all()->where('id_perusahaan', auth()->user()->id_perusahaan);
        $data = SatuanResource::collection($satuan->loadMissing('perusahaan'));

        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSatuanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'nama' => 'required',
                'id_perusahaan' => 'required'
            ]);

            $satuan = Satuan::create([
                'nama' => $request->nama,
                'id_perusahaan' => auth()->user()->id_perusahaan
            ]);
    
            $data = new SatuanResource($satuan->loadMissing('perusahaan'));
            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
        } catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Satuan  $satuan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $satuan = Satuan::where('id', $id)->first();
        $data = new SatuanResource($satuan->loadMissing('perusahaan'));
    
        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Satuan  $satuan
     * @return \Illuminate\Http\Response
     */
    public function edit(Satuan $satuan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSatuanRequest  $request
     * @param  \App\Models\Satuan  $satuan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'nama' => 'required',
                'id_perusahaan' => 'required'
            ]);

            $satuan = Satuan::findOrFail($id);
            $satuan->update([
                'nama' => $request->nama,
                'id_perusahaan' => auth()->user()->id_perusahaan
            ]);

            $data = new SatuanResource($satuan->loadMissing('perusahaan'));
            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
    
        }catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Satuan  $satuan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        DB::beginTransaction();
        try {
            $satuan = Satuan::findOrFail($id);

            $data = $satuan->delete();
            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
        } catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }
    }
}
