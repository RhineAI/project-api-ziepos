<?php

namespace App\Http\Controllers;

use App\Helpers\ApiFormatter;
use App\Models\Kategori;
use App\Models\Perusahaan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\KategoriResource;
use Exception;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = Kategori::all()->where("id_perusahaan", auth()->user()->id_perusahaan);
        $data = KategoriResource::collection($kategori->loadMissing(['perusahaan']));

        if($data) {
            return ApiFormatter::createApi(200, 'Get All Data Berhasil', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'nama' =>'required',
                'id_perusahaan' => 'required'
            ]);

            $kategori = Kategori::create([
                'nama' => $request->nama,
                'id_perusahaan' => auth()->user()->id_perusahaan
            ]);

            $data = new KategoriResource($kategori->loadMissing('perusahaan'));
            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'Input Data Berhasil', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
    
        }catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function show(Kategori $kategori, $id)
    {
        $kategori = Kategori::where('id', $id)->first();
        $data = new KategoriResource($kategori->loadMissing('perusahaan'));
    
        if($data) {
            return ApiFormatter::createApi(200, 'Show Berhasil', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit(Kategori $kategori)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'nama' =>'required',
                'id_perusahaan' => 'required'
            ]);

            $kategori = Kategori::findOrFail($id);

            $kategori->update([
                'nama' => $request->nama,
                'id_perusahaan' => auth()->user()->id_perusahaan
            ]);
        
            $data = new KategoriResource($kategori->loadMissing('perusahaan'));
            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'Update Berhasil', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
    
        } catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $kategori = Kategori::findOrFail($id);

            $data = $kategori->delete();
            DB::commit();
            
            if($data) {
                return ApiFormatter::createApi(200, 'Delete Berhasil', $data);
            } else{
                return ApiFormatter::createApi(400, 'Kesalahan Query ');
            }
        } catch(\Exception $error){
            DB::rollBack();
            return ApiFormatter::createApi(400, 'Kesalahan Query '. $error->getMessage());
        }
    }
}
