<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Kategori;
use App\Models\Perusahaan;
use App\Models\Merek;
use App\Models\Satuan;
use App\Models\User;
use App\Helpers\ApiFormatter;
// use Illuminate\Routing\Controller;
use App\Http\Requests\StoreBarangRequest;
use App\Http\Requests\UpdateBarangRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\BarangResource;
use Exception;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Barang::where("id_perusahaan", auth()->user()->id_perusahaan)->get();
    
        if($data) {
            return BarangResource::collection($data->loadMissing(['kategori', 'supplier', 'satuan', 'merek', 'perusahaan']));
            // return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBarangRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBarangRequest $request)
    {
        try {
            $request->validate([
                'kode'=>'required',
                'nama'=>'required',
                'barcode'=>'required', 
                'id_kategori'=>'required',
                'id_supplier'=>'required',
                'id_satuan'=>'required',
                'id_merek'=>'required',
                'id_perusahaan'=>'required',
                'stock'=>'required',
                'stock_minimal'=>'required',
                'harga_beli'=>'required',
                'keuntungan'=>'required',
                'keterangan'=>'required',
                'status'=>'required'
            ]);

            $barang = Barang::create([
                'kode' => $request->kode,
                'nama'=> $request->nama,
                'barcode'=> $request->barcode, 
                'id_kategori'=> $request->id_kategori,
                'id_supplier'=> $request->id_supplier,
                'id_satuan'=> $request->id_satuan,
                'id_merek'=> $request->id_merek,
                'id_perusahaan'=> auth()->user()->id_perusahaan,
                'stock'=> $request->stock,
                'stock_minimal'=> $request->stock_minimal,
                'harga_beli'=> $request->harga_beli,
                'keuntungan'=> $request->keuntungan,
                'keterangan'=> $request->keterangan,
                'status' => $request->status
            ]);
        
    
            // $data = new BarangResource($barang->loadMissing(['kategori', 'supplier', 'satuan', 'merek', 'perusahaan']));
        
            $perusahaan = Perusahaan::where('id', auth()->user()->id_perusahaan)->first();
            $limit = Barang::whereDate('created_at', date('Y-m-d'))->where('id_perusahaan', auth()->user()->id_perusahaan)->count();
            DB::commit();
        
            if($perusahaan->grade == 1) {
                if($limit < 10 ) {
                    $barang->save();
                    return new BarangResource($barang->loadMissing(['kategori', 'supplier', 'satuan', 'merek', 'perusahaan']));
                }else {
                    return ApiFormatter::createApi(400,'Kamu Telah Mencapai Limit Input Barang');
                }
            } elseif($perusahaan->grade == 2) {
                if($limit < 50 ) {
                    $barang->save();
                    return new BarangResource($barang->loadMissing(['kategori', 'supplier', 'satuan', 'merek', 'perusahaan']));
                }else {
                    return ApiFormatter::createApi(400,'Kamu Telah Mencapai Limit Input Barang');
                }
            } elseif($perusahaan->grade == 3) {
                if($limit < 10000 ) {
                    $barang->save();
                    return new BarangResource($barang->loadMissing(['kategori', 'supplier', 'satuan', 'merek', 'perusahaan']));
                }else {
                    return ApiFormatter::createApi(400,'Kamu Telah Mencapai Limit Input Barang');
                }
            } else{
                return redirect()->route('logout')->with(['error' => 'Kamu Tidak Memiliki Hak Akses']);
            }
        } catch (Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $barang = Barang::where('id', $id)->first();
        $data = new BarangResource($barang->loadMissing(['kategori', 'supplier', 'satuan', 'merek', 'perusahaan']));
    
        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit(Barang $barang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBarangRequest  $request
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBarangRequest $request,$id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'kode'=>'required',
                'nama'=>'required',
                'barcode'=>'required', 
                'id_kategori'=>'required',
                'id_supplier'=>'required',
                'id_satuan'=>'required',
                'id_merek'=>'required',
                'id_perusahaan'=>'required',
                'stock'=>'required',
                'stock_minimal'=>'required',
                'harga_beli'=>'required',
                'keuntungan'=>'required',
                'keterangan'=>'required',
                'status'=>'required'
            ]);

            $barang = Barang::findOrFail($id);

            $barang->update([
                'kode' => $request->kode,
                'nama'=> $request->nama,
                'barcode'=> $request->barcode, 
                'id_kategori'=> $request->id_kategori,
                'id_supplier'=> $request->id_supplier,
                'id_satuan'=> $request->id_satuan,
                'id_merek'=> $request->id_merek,
                'id_perusahaan'=> auth()->user()->id_perusahaan,
                'stock'=> $request->stock,
                'stock_minimal'=> $request->stock_minimal,
                'harga_beli'=> $request->harga_beli,
                'keuntungan'=> $request->keuntungan,
                'keterangan'=> $request->keterangan,
                'status' => $request->status
            ]);
        
            $data = new BarangResource($barang->loadMissing(['kategori', 'supplier', 'satuan', 'merek', 'perusahaan']));
            DB::commit();
        
            if($data) {
                return ApiFormatter::createApi(200, 'Update Data Berhasil', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
    
        }catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $barang = Barang::findOrFail($id);

            $data = $barang->delete();
            DB::commit();
            
            if($data) {
                return ApiFormatter::createApi(200, 'Data Berhasil Dihapus', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
        } catch (Exception $error){
            DB::rollBack();
            return ApiFormatter::createApi(400,'Data Tidak ada ' . $error->getMessage());
        }
        
    }
}
