<?php

namespace App\Http\Controllers;

use App\Models\user;
use App\Models\Perusahaan;
use App\Helpers\ApiFormatter;
// use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StorePerusahaanRequest;
use App\Http\Requests\UpdatePerusahaanRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\PerusahaanResource;
use App\Http\Resources\UserResource;
use Exception;
use Illuminate\Support\Facades\DB;

class PerusahaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data = Perusahaan::all()->where('id', auth()->user()->id_perusahaan);

        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePerusahaanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePerusahaanRequest $request)
    {
        DB::beginTransaction();
        try{
            $validasi = Validator::make($request->all(), [
                'nama' =>'required',
                'alamat' => 'required',
                'tlp' => 'required|min:11',
                'pemilik' => 'required',
                'bank' => 'required',
                'no_rekening' => 'required|min:10',
                'npwp' => 'required',
                'slogan' => 'required',
                'email' => 'required',
            ]);
    
    
            $perusahaan = Perusahaan::create([
                'nama' =>$request->nama,
                'alamat' => $request->alamat,
                'tlp' =>$request->tlp,
                'pemilik' => $request->pemilik,
                'bank' => $request->bank,
                'no_rekening' => $request->no_rekening,
                'npwp' =>$request->npwp,
                'slogan' => $request->slogan,
                'email' =>$request->email,
                'logo' => $request->logo,
                'grade' => $request->grade,
                'startDate' => date('Y-m-d')
            ]);
    
            $User = User::create([
                'nama' =>$perusahaan->nama,
                'alamat' => $perusahaan->alamat,
                'tlp' =>  $perusahaan->tlp,
                'jenis_kelamin' => 'Other',
                'id_perusahaan' => $perusahaan->id,
                'username' => str_replace(' ', '', $perusahaan->nama),
                'password' => bcrypt(str_replace(' ', '', strtolower($perusahaan->pemilik).'123')),
                'hak_akses' => "owner"
            ]);
            $dataUser = new UserResource($User);
            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Registered successfully',
                'perusahaan' => [
                    'dataPerusahaan' => $perusahaan,
                    'dataOwner' => $dataUser
                ]
                // ,'authorisation' => [
                //     'token' => $User->createToken('Auth Token')->plainTextToken,
                //     'type' => 'bearer',
                // ]
            ]);
        }catch(\Exception $error){
            DB::rollBack();
            return response()->json([
                'status' => 'failed',
                'message' => 'Register Failed Because'. $error->getMessage()
            ], 500);
        }
    }

    public function show($id)
    {
        $data = Perusahaan::where('id', $id)->first();
    
        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Perusahaan  $perusahaan
     * @return \Illuminate\Http\Response
     */
    public function edit(Perusahaan $perusahaan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePerusahaanRequest  $request
     * @param  \App\Models\Perusahaan  $perusahaan
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePerusahaanRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            // $request->validate([
            //     'nama' =>'required',
            //     'alamat' => 'required',
            //     'tlp' => 'required',
            //     'pemilik' => 'required',
            //     'bank' => 'required',
            //     'no_rekening' => 'required',
            //     'npwp' => 'required',
            //     'slogan' => 'required',
            //     'email' => 'required',
            //     'logo' => 'required',
            //     'grade' => 'required'
            // ]);

            $perusahaan = Perusahaan::findOrFail($id);
            

            $perusahaan->update($request->all()); 
    
            $data = Perusahaan::where('id', $perusahaan->id)->get();
            DB::commit();
            
            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
    
        }catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Perusahaan  $perusahaan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $perusahaan = Perusahaan::findOrFail($id);

            $data = $perusahaan->delete();
            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'success', $perusahaan);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
        }catch(\Exception $error){
            DB::rollBack();
            return ApiFormatter::createApi(500, 'Kesalahan '. $error->getMessage());
        }
    }

    public function error($message) {
        return response()->json([
            'Code' => 500,
            'Message' => $message
        ]);
     }

    
}
