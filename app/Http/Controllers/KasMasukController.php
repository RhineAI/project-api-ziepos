<?php

namespace App\Http\Controllers;

use App\Helpers\ApiFormatter;
use App\Models\KasMasuk;
use App\Http\Controllers\Controller;
use App\Http\Resources\KasMasukResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KasMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = kasMasuk::all()->where('id_perusahaan', auth()->user()->id_perusahaan);

        if($data) {
            return KasMasukResource::collection($data->loadMissing(['user', 'perusahaan']));
            // return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada DAta');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreKasMasukControllerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'tgl' => 'required',
                'jumlah' => 'required|numeric',
                'keterangan' => 'required'
            ]);


            $kasMasuk = kasMasuk::create([
                'tgl' => $request->tgl,
                'jumlah' => $request->jumlah,
                'keterangan' => $request->keterangan,
                'id_user' => auth()->user()->id,
                'id_perusahaan' => auth()->user()->id_perusahaan,
            ]);
    
            $data = new KasMasukResource($kasMasuk->loadMissing(['user', 'perusahaan']));
            DB::commit();
        
            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query ');
            }
    
        }   catch (Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KasMasukController  $kasMasukController
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kasMasuk = KasMasuk::where('id', $id)->first();
        $data = new KasMasukResource($kasMasuk->loadMissing(['user', 'perusahaan']));
    
        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Data Tidak Ditemukan');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KasMasukController  $kasMasukController
     * @return \Illuminate\Http\Response
     */
    public function edit(KasMasukController $kasMasukController)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateKasMasukControllerRequest  $request
     * @param  \App\Models\KasMasukController  $kasMasukController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'tgl' => 'required',
                'jumlah' => 'required',
                'keterangan' => 'required'
            ]);

            $kasMasuk = kasMasuk::findOrFail($id);

            $kasMasuk->update([
                'tgl' => $request->tgl,
                'jumlah' => $request->jumlah,
                'keterangan' => $request->keterangan,
                'id_user' => auth()->user()->id,
                'id_perusahaan' => auth()->user()->id_perusahaan,
            ]);
    
            $data = new KasMasukResource($kasMasuk->loadMissing(['user', 'perusahaan']));
            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'Update Berhasil', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
    
        }catch (Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }          
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $kasMasuk = KasMasuk::findOrFail($id);

            $data = $kasMasuk->delete();
            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query ');
            }
        } catch (Exception $error){
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }
        
    }
}
