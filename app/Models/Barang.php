<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;
    protected $table = 't_barang';
    protected $fillable = ['kode', 'nama', 'barcode', 'id_kategori', 'id_supplier', 'id_satuan', 'id_merek', 'id_perusahaan', 
    'stock', 'stock_minimal', 'harga_beli', 'keuntungan', 'keterangan', 'status'];
    protected $primaryKey = 'id';

    // private $key = 'modifier';

    public function kategori(){
        return $this->belongsTo(Kategori::class, 'id_kategori', 'id');
    }

    public function supplier(){
        return $this->belongsTo(Supplier::class, 'id_supplier', 'id');
    }

    public function satuan(){
        return $this->belongsTo(Satuan::class, 'id_satuan', 'id');
    }

    public function merek(){
        return $this->belongsTo(Merek::class, 'id_merek', 'id');
    }
    
    public function perusahaan(){
        return $this->belongsTo(Perusahaan::class, 'id_perusahaan', 'id');
    }
}
