<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KasMasuk extends Model
{
    use HasFactory;

    protected $table = 't_kas_masuk';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class, 'id_user', 'id');
    }

    public function perusahaan(){
        return $this->belongsTo(Perusahaan::class, 'id_perusahaan', 'id');
    }
}

