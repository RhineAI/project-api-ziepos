<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class laporanBug extends Model
{
    use HasFactory;
    protected $table = 'laporanbug';
    protected $fillable = ['jenis', 'deskripsi', 'tglKejadian', 'pelapor', 'statuses', 'id_perusahaan'];
    protected $primaryKey = 'id';
}
